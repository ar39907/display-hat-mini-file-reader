 # Display Hat Mini File Reader (2022)



## Description

Archived Display HAT Mini File Reader project from 2022.


## Getting Started

### Dependencies

* Python, Ncurses library, C Standard Library, G++
* Raspberry Pi, Display HAT Mini


### Usage

* Connect Display HAT Mini to Raspberry Pi
* Compile feedback.cpp
* Run executable
* (Optional) Run feedback.cpp executable from reboot


## Author

Austin Richie

https://gitlab.com/ar39907/
